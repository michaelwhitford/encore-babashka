# minimal reproduction

Encore does not work under babashka.

See the error:

```
bb --debug test.cljc
```

But it does work fine in clojure:

```
clojure -M test.cljc
```
