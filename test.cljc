(ns test
  (:require [taoensso.encore :as enc]
            [babashka.fs :as fs]))

; this should fail because test.txt does not exist relative to the test.cljc file
(enc/catching (slurp (str (fs/parent *file*) "/test.txt")))
